# Topology_isomorphism_gmx

Divide a protein structure into non overlapping moieties according to a custom predefined list.

## How does it work?

The target structure is converted to an undirected graph, where edge and vertex properties are inferred from a Gromacs topology file.
Each predefined moiety is then searched in order and each matching atom is removed recursively from the structure.

## How to cite?

This code has been used in:

Toward a unified scoring function for native state discrimination and drug-binding pocket recognition

A Battisti, S Zamuner, E Sarti, A Laio

Physical Chemistry Chemical Physics 20 (25), 17148-17155
