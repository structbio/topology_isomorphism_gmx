import sys

def smiles(graph,types,root=1):
  n_hydrogens=0
  branches=[]
  for c in graph[root]:
    if types[c][0]=='H':
      n_hydrogens+=1
    else:
      branches.append(c)
  if n_hydrogens!=0:
    s="["+types[root][0]+"H"+str(n_hydrogens)+"]"
  else:
    s="["+types[root][0]+"]"
  if len(branches) > 0:
    for b in branches[:]:
      s+="("+smiles(graph,types,b)+")"
  return s

def chemfig(graph,types,root=1):
  n_hydrogens=0
  branches=[]
  for c in graph[root]:
    if types[c][0]=='H':
      n_hydrogens+=1
    else:
      branches.append(c)
  s=types[root]
  n_branches = len(branches)
  angle=[]
  if n_branches==1:
    angle=["0"]
  elif n_branches==2:
    angle=["::45","::-45"]
  elif n_branches==2:
    angle=["::90","::-90"]
  if len(branches) > 0:
    for idx,b in enumerate(branches):
      s+="(["+angle[idx]+"]-"+chemfig(graph,types,b)+")"
  return s

def readTopologyFile(filename):
  graph={}
  types={}
  with open(filename,'r') as topo_file:
    is_atom=False
    is_bond=False
    for line in topo_file:
      if len(line)<2:
        continue
      if line[:9] == "[ atoms ]":
        is_atom=True
        is_bond=False
        continue
      if line[:9] == "[ bonds ]":
        is_atom=False
        is_bond=True
        continue
      if line[0] == "[":
        is_atom=False
        is_bond=False
        continue
      if is_atom:
        idx,atm_type=line.split()[0:2]
        idx=int(idx)
        types[idx]=atm_type
        graph[idx]=[]
      if is_bond:
        idx1,idx2=line.split()[0:2]
        idx1=int(idx1)
        idx2=int(idx2)
        graph[idx1].append(idx2)
  return graph,types
  
if __name__ == "__main__":
  import sys
  filename=sys.argv[1]
  out_format=sys.argv[2]
  
  s=""
  graph,types = readTopologyFile(filename)
  if out_format=="chemfig":
    s="\chemfig{" + chemfig(graph,types)+"}"
  elif out_format=="smiles":
    s=smiles(graph,types)
  else:
    sys.exit()
    
  print(s)
