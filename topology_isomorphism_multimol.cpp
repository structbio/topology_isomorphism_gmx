#include <fstream>
#include <ios>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <Eigen/Core>

#include <boost/algorithm/string.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/vf2_sub_graph_iso.hpp>
#include <boost/graph/connected_components.hpp>
#ifndef BOOST_GRAPH_ITERATION_MACROS_HPP
#define BOOST_ISO_INCLUDED_ITER_MACROS // local macro, see bottom of file
#include <boost/graph/iteration_macros.hpp>
#endif

const uint moiname_len = 4;

struct atom{
  std::string header;             /*!< "ATOM  " or "HETATM" */
  int serial;                     /*!< serial (progressive) number */
  std::string id;                 /*!< atom identifier (CA, CB, ...) */
  char altLoc;                    /*!< alternative location */
  std::string resName;            /*!< residue name */
  char chainId;                   /*!< chain identifier */
  int resSeq;                     /*!< residue number */
  char iCode;                     /*!< code for insertion of residues */
  Eigen::Vector3d coordinate;     /*!< coordinate */
  double occupancy;               /*!< occupancy */
  double tempFactor;              /*!< temperature factor */
  std::string element;            /*!< element type (carbon, oxygen, ...) */
  double charge;                  /*!< charge */
};

std::ostream& operator << ( std::ostream& out, atom& atm ){
  out << atm.header;

  if(atm.element=="  "){
    atm.element[1]=atm.id[1];
  }
  out << std::setw(5) << atm.serial;
  out << " ";
  out << std::setw(4) << atm.id;
  out << atm.altLoc;
  out << std::setw(3) << atm.resName;
  out << " ";
  out << atm.chainId;
  out << std::setw(4) << atm.resSeq;
  out << atm.iCode;
  out << "   ";
  out << std::fixed << std::setprecision(3);
  out << std::setw(8) << atm.coordinate(0);
  out << std::setw(8) << atm.coordinate(1);
  out << std::setw(8) << atm.coordinate(2);
  out << std::fixed << std::setprecision(2);
  out << std::setw(6) << atm.occupancy;
  out << std::setw(6) << atm.tempFactor;
  out << "          "; // blank
  out << atm.element;
  if( atm.charge == 0. )
    out << "  ";
  else{
    out << std::setprecision(0) << fabs(atm.charge);
    if( atm.charge > 0 )
      out << '+';
    else if(atm.charge < 0){
      out << '-';
    }
  }
  out << std::endl;
  return out;
};

atom parseAtom(std::string& line){
  atom info;
  auto line_id = line.substr(0,6);
  if(line_id == std::string("ATOM  ") or line_id == std::string("HETATM") ){
    info.header=line_id;
    info.serial = atoi(line.substr(6, 5).c_str()); 
    info.id = line.substr(12, 4);
    info.altLoc = line.substr(16, 1).c_str()[0];
    info.resName = line.substr(17, 3);
    info.chainId = isdigit( line.substr(21, 1).c_str()[0] ) ? char( 'A'+atoi(line.substr(21, 1).c_str()) ) : line.substr(21, 1).c_str()[0];
    if(info.chainId == ' ') info.chainId = 'A';
    info.chainId = toupper(info.chainId);
    info.resSeq = atoi( line.substr(22, 4).c_str() );
    info.iCode = line.substr(26, 1).c_str()[0];
    info.coordinate = Eigen::Vector3d( atof(line.substr(30, 8).c_str()), 
                                       atof(line.substr(38, 8).c_str()),
                                       atof(line.substr(46, 8).c_str()));
  
    try{
      info.occupancy = atof(line.substr(54, 6).c_str()) ? atof(line.substr(54, 6).c_str()) : 0.;
    }
    catch(std::out_of_range& oor){
      info.occupancy = 0.0;
    }
  
    try{
      info.tempFactor = atof(line.substr(60, 6).c_str()) ? atof(line.substr(60, 6).c_str()) : 0.;
    }
    catch (std::out_of_range& oor){
      info.tempFactor = 0.0;
    }
    try{
      info.element = line.substr(76,2);
    }
    catch (std::out_of_range& oor){
      info.element = " H";
    }
    info.charge = 0;
    try {
      if( isdigit( line.substr(78, 2).c_str()[0] ) and ( line.substr(78, 2).c_str()[1]=='-' or line.substr(78, 2).c_str()[1]=='+') ){
        info.charge = line.substr(78, 2).c_str()[1]=='-' ? -atof(line.substr(78, 1).c_str()) : atof(line.substr(78, 1).c_str());
      }
    }
    catch (std::out_of_range& oor){
      info.charge = 0;
    }
  }
  return info;
};

std::string tail(std::string source, size_t const length) {
  if (length >= source.size()) { return source; }
  return source.substr(source.size() - length);
}

std::string getIncludeFilename (std::string& str){
  std::size_t first = str.find_first_of('"');
  std::size_t last = str.find_last_of('"');
  return str.substr(first+1,last-first-1);
}

struct vertex_t{ 
  uint id; 
  std::string type;
  vertex_t(uint s, std::string t): id(s),type(t){}
  vertex_t(){};
};

struct edge_t{
  uint type;
  edge_t(uint t):type(t){}
  edge_t(){};
};

class topology_t{
  public:
    typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS, vertex_t, edge_t> graph_t;
    typedef typename boost::graph_traits<graph_t>::vertex_descriptor vertex_descriptor;
    typedef typename boost::graph_traits<graph_t>::edge_descriptor edge_descriptor;
    typedef typename boost::graph_traits<graph_t>::vertex_iterator vertex_iterator;
    typedef typename boost::graph_traits<graph_t>::edge_iterator edge_iterator;
  private:
    graph_t G;
    std::map< uint,vertex_descriptor > v_to_descriptor;
    std::map< std::pair<uint,uint>, edge_descriptor > e_to_descriptor;
    void addVertex(uint id, vertex_t vertex){
      auto v = boost::add_vertex(G);
      G[v]=vertex;
      v_to_descriptor[ id ] = v;
      ++n_vertices;
    }
    void addEdge(uint v1, uint v2, edge_t edge){
      edge_descriptor e;
      bool b;
      boost::tie(e,b) = boost::add_edge(v_to_descriptor[v1],v_to_descriptor[v2],G);
      G[e]=edge;
      e_to_descriptor[ std::make_pair(v1,v2) ] = e;
      e_to_descriptor[ std::make_pair(v2,v1) ] = e;
    }
  public:
    uint n_vertices;
    std::string basefolder;
    
    graph_t& getGraph(){
      return G;
    }
    
    // return the vertex associated to a given serial
    vertex_t& getVertex(uint id){
      return G[ v_to_descriptor[id] ];
    }
    
    // return the edge between two vertices
    edge_t& getEdge(uint v1, uint v2){
      return G[ e_to_descriptor[ std::make_pair(v1,v2) ] ];
    }
    
    void readFromFile( std::string top_filename){
      uint offset=n_vertices;
      std::vector<std::string> itp_list;
      std::ifstream top_file;
      top_file.exceptions(std::ios::failbit);
      try {
        top_file.open(top_filename, std::ios::in);
      }
      catch (...) {
        return;
      }
      top_file.exceptions(std::ios::goodbit);
      
      std::string line;
      bool is_atom=false,is_bond=false,skipline=true;
      while(std::getline(top_file, line)){
        if(line[0]==';' or line.size()<3){ // skip comments and empty lines
          continue;
        }
        if(line.substr(0,8)=="#include" and tail(line,15)!="forcefield.itp\""){
          itp_list.push_back( basefolder+getIncludeFilename(line) );
        }
        if(line.substr(0,9)=="[ atoms ]"){ // look for 'atoms' section
          is_atom=true,is_bond=false,skipline=false;
          continue;
        }
        else if(line.substr(0,9)=="[ bonds ]"){ // look for 'bonds' section
          is_atom=false,is_bond=true,skipline=false;
          continue;
        }
        else if(line.substr(0,1)=="["){ // skip other sections
          is_atom=false,is_bond=false,skipline=true;
          continue;
        }
        if(skipline) continue;
        
        if(is_atom){ // add atom to topology
          uint serial;
          std::string type;
          std::istringstream s_line(line);
          s_line >> serial >> type;
          vertex_t v(serial+offset,type);
          addVertex(serial+offset,v);
        }
        else if(is_bond){ // add bond to topology
          uint serial1, serial2;
          std::istringstream s_line(line);
          s_line >> serial1 >> serial2;
          edge_t e(0);
          addEdge(serial1+offset,serial2+offset,e);
        }
      }
      for(auto itp: itp_list){
        readFromFile(itp);
      }
      top_file.close();
    }
    
    void readFromFile( const char* top_filename){
      readFromFile( std::string(top_filename) );
    }
    
    // build a topology from *.top file
    topology_t( const char* top_filename){
      n_vertices=0;
      std::string filename = std::string(top_filename);
      uint end = (filename.substr(0,filename.find_first_of('/'))).size();
      if(end<filename.size()){
        basefolder=filename.substr(0,filename.find_last_of('/'))+"/";
      }
      else{
        basefolder="";
      }
      readFromFile(filename);
    }
    
};

struct graph_equivalent{
  const topology_t::graph_t& graphSmall;
  const topology_t::graph_t& graphLarge;
  
  std::set<topology_t::vertex_descriptor>& used_vertices;
  std::vector< std::vector<topology_t::vertex_descriptor> >& subgraphs;
  
  graph_equivalent( topology_t::graph_t& graph_small, topology_t::graph_t& graph_large, 
                    std::set<topology_t::vertex_descriptor>& used, 
                    std::vector< std::vector<topology_t::vertex_descriptor> >& sub ):  
    graphSmall(graph_small), graphLarge(graph_large), used_vertices(used), subgraphs(sub) {};
   
  bool operator()(const topology_t::vertex_descriptor& v1, const topology_t::vertex_descriptor& v2){
    if( graphSmall[v1].type != graphLarge[v2].type ){
      return false;
    }
    return used_vertices.find( v2 )==used_vertices.end();
  }
  
  bool operator()(const topology_t::edge_descriptor& e1, const topology_t::edge_descriptor& e2){
//    return true;
    return graphSmall[e1].type==graphLarge[e2].type;
  }

  template <typename CorrespondenceMap1To2, typename CorrespondenceMap2To1>
  bool operator()(CorrespondenceMap1To2 f, CorrespondenceMap2To1){
    topology_t::vertex_iterator v_beg, v_end;
    boost::tie(v_beg, v_end) = boost::vertices( graphSmall );
    std::vector<topology_t::vertex_descriptor> cur;
    for( topology_t::vertex_iterator v = v_beg; v != v_end; ++v ){
      auto fv = boost::get(f,*v);
      if( used_vertices.find( fv )==used_vertices.end() ){
        used_vertices.insert( fv );
        cur.push_back( fv );
      }
    }
    if( cur.size() ){
      subgraphs.push_back(cur);
    }
    return true;
  }
};

std::string make_label( atom& atm){
  std::string label="";
  if(atm.header=="ATOM  "){
    label.append(1,atm.chainId);
  }
  else{
    label=atm.resName;
    label.append(1,atm.chainId);
    label+=std::to_string(atm.resSeq);
  }
  return label;
}

int main(int argc, char* argv[]){
  // read pdb file
  const char* pdb_filename = argv[1];
  std::ifstream pdb_file(pdb_filename);
  std::string line;
  std::map<uint,atom> atom_list;
  while(std::getline(pdb_file, line)){
    auto line_id = line.substr(0,6);
    if(line_id == std::string("ATOM  ") or line_id == std::string("HETATM") ){
      auto atm = parseAtom(line);
      auto serial = atm.serial;
      atom_list[serial]=atm;
    }
  }
  pdb_file.close();
  
  // Build topology from *.top file
  const char* top_filename = argv[2];
  topology_t top(top_filename);
  
  // Get connected components from topology. These will be considered as different molecules
  std::vector<int> vertexid_to_component(top.n_vertices);
  int n_mols = boost::connected_components(top.getGraph(), &vertexid_to_component[0]);
  std::vector< std::vector<int> > component_members(n_mols); // map component index with vertex id
  std::vector<std::string> component_label(n_mols);
  for(uint i=0; i!=top.n_vertices; ++i){
    int component_id = vertexid_to_component[i];
    if ( component_members[ component_id ].size()==0 ){
      int serial = top.getGraph()[i].id;
      component_label[component_id] = make_label(atom_list[serial]);
    }
    component_members[ component_id ].push_back(i);
  }
  
  
  // read moiety topologies and store them in a vector
  std::vector<topology_t> moieties;
  const char* moi_filename = argv[3];
  std::string moi_filename_str = std::string(moi_filename);
  std::string moi_basedir = moi_filename_str.substr(0,moi_filename_str.find_last_of('/'))+"/";
  std::ifstream moi_file(moi_filename);
  std::vector<std::string> moiname;
  std::map<std::string,std::string> moiname2key;
  while(std::getline(moi_file, line)){
    if(line[0]=='#'){ // skip comments
      continue;
    }
    boost::algorithm::trim_right(line);
    std::string moikey=line.substr(0,moiname_len);
    std::string descr_filename = line.substr(moiname_len+1,1000);
    if(descr_filename[0]!='/'){
      descr_filename=moi_basedir+descr_filename;
    }
    std::vector<std::string> strs;
    boost::split(strs,descr_filename,boost::is_any_of("/."));
    moiname.push_back( strs[ strs.size()-2 ] );
    moiname2key[moiname.back()] = moikey;
    moieties.push_back( topology_t(descr_filename.c_str()) );
  }  
  // now check subgraph isomorphisms
  std::set<topology_t::vertex_descriptor> used_vertices;
  std::map<std::string,std::vector<std::pair< uint, std::vector<topology_t::vertex_descriptor>>>> subdivision;
  std::map<std::string,uint> atoms_per_molecule;
  uint moi_idx = 0;
  for(auto moiety_top: moieties){
    std::vector< std::vector<topology_t::vertex_descriptor> > subgraphs;
    graph_equivalent eq_g(moiety_top.getGraph(), top.getGraph(), used_vertices, subgraphs);
    boost::vf2_subgraph_iso( 
                             moiety_top.getGraph(), 
                             top.getGraph(), 
                             eq_g,
                             boost::get(boost::vertex_index, moiety_top.getGraph()),
                             boost::get(boost::vertex_index, top.getGraph()),
                             boost::vertex_order_by_mult( moiety_top.getGraph() ),  
                             eq_g, 
                             eq_g);
    for(auto subgraph: subgraphs){
      std::string molecule_id = component_label[ vertexid_to_component[ *subgraph.begin() ] ];
      if(subdivision.find(molecule_id)==subdivision.end()){
        subdivision[molecule_id]={};
        atoms_per_molecule[molecule_id]=0;
      }
      subdivision[molecule_id].push_back(std::make_pair( moi_idx,subgraph ));
      atoms_per_molecule[molecule_id]+=subgraph.size();
    }
    ++moi_idx;
  }
  
  // print number of molecules and moieties per molecule
  std::cout << "N_MOLS  "  << subdivision.size() << std::endl;                                  // number of non-empty moleculess
  for(auto c: subdivision){
    std::cout << "NMOI    " << c.first << " " << std::setw(5) << c.second.size() << std::endl;  // number of moieties per molecule
  }
  // print unknown atoms
  int unused = top.n_vertices - used_vertices.size();
  std::cout << "UNKATM  " << unused << std::endl;                                               // number of unknown atoms. this are atoms that have not be assigned to any moiety
  if(unused){                                                                                   // for each unknown atom also print some info
    std::set<unsigned int> serials;
    for( auto v: used_vertices){
      serials.insert( top.getGraph()[v].id );
    }
    for(auto a: atom_list){
      if( serials.find(a.first)==serials.end() ){
        std::cout << a.second;
      }
    }
  }
  // print moiety list
  for( auto sub: subdivision ){ // sub:(char,vec< pair< int, vec<vertex> > >)
    std::cout << "BEGMOL  " << sub.first << std::endl;
    for(auto p: sub.second){ // p:pair< int, vec<vertex> >
      std::cout << "MOIETY  " << std::setw(moiname_len) << moiname2key[ moiname[p.first] ] << " " 
                              << p.second.size()
                              << std::endl;
      for( auto v: p.second ){
        std::cout << atom_list[ top.getGraph()[v].id ];
      }
    }
    std::cout << "ENDMOL" << std::endl;
  }
  return 0;
}
